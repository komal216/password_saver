import 'package:flutter/material.dart';
import 'package:flutter_application_2/model/password_model.dart';

class Profile extends StatefulWidget {
  final String name;
  final String password;

  const Profile({
    this.name = '',
    this.password = '',
  });

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  TextEditingController nameController;

  TextEditingController passwordController;

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController(text: widget.name);
    passwordController = TextEditingController(text: widget.password);
    // Function for get product details
  }

  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/12.jpg"),
          fit: BoxFit.cover,
        ),
      ),
      height: height,
      width: width,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: width,
              height: height * 0.45,
              child: Image.asset(
                'assets/images/12.jpg',
                fit: BoxFit.fill,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Login',
                    style:
                        TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 30.0,
            ),
            TextField(
              controller: nameController,
              decoration: InputDecoration(
                hintText: 'Email',
                suffixIcon: Icon(Icons.email),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            TextField(
              controller: passwordController,
              obscureText: true,
              decoration: InputDecoration(
                hintText: 'Password',
                suffixIcon: Icon(Icons.visibility_off),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
              ),
            ),
            SizedBox(
              height: 30.0,
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  RaisedButton(
                    child: Text('Save'),
                    color: Colors.white38,
                    onPressed: () {
                      print(nameController.text);
                      print(passwordController.text);

                      PasswordModel model = PasswordModel(
                          nameController.text.trim(),
                          passwordController.text.trim());
                      Navigator.of(context).pop(model);
                    },
                  ),
                  // RaisedButton(
                  //   child: Text('Edit'),
                  //   color: Colors.white38,
                  //   onPressed: () {
                  //     print(nameController.text);
                  //     print(passwordController.text);
                  //   },
                  // ),
                ],
              ),
            ),
          ],
        ),
      ),
    ));

    // Widget _getContainer(Color color) {
    //   return Container(
    //     child: Row(
    //       children: [
    //         Text('Komal'),
    //         Container(
    //           height: 80,
    //           width: 60,
    //           color: Colors.greenAccent,
    //         ),
    //       ],
    //     ),
    //     height: 150,
    //     width: 150,
    //     color: color,
    //   );
    // }

    // Widget _getTopView() {
    //   return Column(
    //       // crossAxisAlignment: CrossAxisAlignment.start,
    //       // children: [
    //       // crossAxisAlignment: CrossAxisAlignment.start,
    //       children: [
    //         Padding(
    //           padding: const EdgeInsets.only(top: 8.0),
    //           child: Icon(Icons.arrow_back),
    //         ),
    //         Padding(
    //           padding: const EdgeInsets.only(left: 120, top: 30),
    //           child: CircleAvatar(
    //             // backgroundColor: Colors.greenAccent[400],
    //             backgroundImage: AssetImage('assets/images/pink.jpg'),
    //             radius: 60,
    //             // child: Image.asset('assets/images/pink.jpg'),
    //           ),
    //         ),
    //         Column(
    //           children: [
    //             Row(
    //               children: [
    //                 Icon(Icons.person),
    //                 Text('Mark Shaw'),
    //               ],
    //             ),
    //             Row(
    //               children: [
    //                 Icon(Icons.inbox),
    //                 Text('sales@shawmansoftware.com'),
    //               ],
    //             ),
    //             Row(
    //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //               // crossAxisAlignment: CrossAxisAlignment.end,
    //               children: [
    //                 Padding(
    //                   padding: const EdgeInsets.only(left: 2.0),
    //                   child: Row(
    //                     children: [
    //                       Icon(Icons.mobile_friendly),
    //                       Text('9789876533'),
    //                     ],
    //                   ),
    //                 ),
    //                 Padding(
    //                   padding: const EdgeInsets.only(right: 3.0),
    //                   child: Row(
    //                     children: [
    //                       Icon(Icons.calendar_today_rounded),
    //                       Text('01/01/1990'),
    //                     ],
    //                   ),
    //                 ),
    //               ],
    //             ),
    //           ],
    //         )
    //       ]);

    //     children: [
    //       Row(
    //         children: [
    //           Icon(Icons.person),
    //           Text('Mark Shaw'),
    //         ],
    //       ),
    //       Row(
    //         children: [
    //           Icon(Icons.inbox),
    //           Text('sales@shawmansoftware.com'),
    //         ],
    //       ),
    //       Row(
    //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //         // crossAxisAlignment: CrossAxisAlignment.end,
    //         children: [
    //           Padding(
    //             padding: const EdgeInsets.only(left: 2.0),
    //             child: Row(
    //               children: [
    //                 Icon(Icons.mobile_friendly),
    //                 Text('9789876533'),
    //               ],
    //             ),
    //           ),
    //           Padding(
    //             padding: const EdgeInsets.only(right: 3.0),
    //             child: Row(
    //               children: [
    //                 Icon(Icons.calendar_today_rounded),
    //                 Text('01/01/1990'),
    //               ],
    //             ),
    //           ),
    //         ],
    //       ),
    //     ],
    //   )
    // ],
    // }
  }
}
