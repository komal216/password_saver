import 'package:flutter/material.dart';
import 'package:flutter_application_2/utils/session_manager.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  void initState() {
    super.initState();
    _checkIsUserLogin();
  }

  bool isUserLoggedIn = false;

  _checkIsUserLogin() async {
    SessionManager manager = await SessionManager.getInstance();
    // if (manager.getPassword().isNotEmpty) {
    //   isUserLoggedIn = true;
    // }else{
    //   isUserLoggedIn = false;
    // }

    isUserLoggedIn = manager.getPassword().isNotEmpty;
  }

  TextEditingController _loginPassword = TextEditingController();
  TextEditingController _crePassword = TextEditingController();
  TextEditingController _confirmPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: isUserLoggedIn ? _passwordInput() : _createPassword(),
      ),
    );
  }

  int getInt(int i) {
    // if (i == 10) {
    //   return i;
    // } else {
    //   return 20;
    // }

    return i == 10 ? i : 20;
  }

  _passwordInput() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text("Enter Your Password"),
        TextField(
          controller: _loginPassword,
        ),
      ],
    );
  }

  _createPassword() {
    return Column(
      children: [
        Text("Enter Your Password"),
        TextField(
          controller: _crePassword,
        ),
        Text("Confirm password"),
        TextField(
          controller: _confirmPassword,
        ),
        ElevatedButton(
          onPressed: () {},
          child: Text("Save"),
        )
      ],
    );
  }
}
