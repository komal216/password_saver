import 'package:flutter/material.dart';
import 'package:flutter_application_2/model/password_model.dart';
import 'profile.dart';

class First extends StatefulWidget {
  @override
  _FirstState createState() => _FirstState();
}

class _FirstState extends State<First> {
  List<PasswordModel> lstPasswords = [
    // PasswordModel("abc.com", "werersdf"),
    // PasswordModel("xyz.com", "werersdf"),
    // PasswordModel("kbc.com", "komal")
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigo[300],
      appBar: AppBar(
        backgroundColor: Colors.indigo,
      ),
      body: Center(
        child: ListView.builder(
          itemCount: lstPasswords.length,
          itemBuilder: (context, index) {
            return Container(
              height: 60,
              child: InkWell(
                onTap: () async {
                  PasswordModel model = await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Profile(
                              name: lstPasswords[index].name,
                              password: lstPasswords[index].password,
                            )),
                  );
                  // lstPasswords.add(model);
                  lstPasswords[index].name = model.name;
                  lstPasswords[index].password = model.password;
                  setState(() {});
                },
                child: Card(
                  elevation: 4.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  color: Colors.white24,
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Text('Name: '),
                            ),
                            Text(lstPasswords[index].name),
                          ],
                        ),
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Text('Password: '),
                            ),
                            Text(lstPasswords[index].password),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        // isExtended: true,
        child: Icon(Icons.add),
        backgroundColor: Colors.indigo,
        onPressed: () {
          setState(() async {
            final model = await Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Profile()),
            );
            lstPasswords.add(model);
            setState(() {});
          });
        },
      ),
    );
    // appBar: AppBar(),
    // body: Container(
    //   decoration: BoxDecoration(
    //     image: DecorationImage(
    //       image: AssetImage("assets/images/12.jpg"),
    //       fit: BoxFit.cover,
    //     ),
    //   ),
    // )
    // ,
  }
}
