import 'package:shared_preferences/shared_preferences.dart';

class SessionManager {
  static SessionManager _manager;
  static SharedPreferences _prefs;

  static Future<SessionManager> getInstance() async {
    if (_manager == null || _prefs == null) {
      _manager = SessionManager();
      _prefs = await SharedPreferences.getInstance();
    }
    return _manager;
  }

  String getPassword() {
    return _prefs.getString("KEY_PASSWORD") ?? "";
  }

  void putPassword(String pass) {
    _prefs.setString("KEY_PASSWORD", pass);
  }
}
