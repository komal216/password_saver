import 'package:flutter/material.dart';

import 'pages/profile.dart';
import 'pages/first.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: First(),
      // home: First(),
    );
  }
}

// Route routing(RouteSettings settings) {
//     switch (settings.name) {
//       case '/mainscreen':
//         return PageTransition(
//             child: Profile(),
//             type: PageTransitionType.rightToLeftWithFade);
//         break;
// //  case '/terms_policy':
// //         return PageTransition(
// //             child: TermsPolicy(), type: PageTransitionType.rightToLeftWithFade);
// //         break;
//       default:
//         return PageTransition(
//             child: First(), type: PageTransitionType.rightToLeftWithFade);
//         break;
//     }
//   }
